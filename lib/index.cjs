const { all, rules } = require('./rules.cjs');

module.exports = {
  configs: {
    all,
  },
  rules,
};
